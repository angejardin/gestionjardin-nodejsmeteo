const mongoose = require('mongoose');

const meteoSchema = mongoose.Schema({
    insee: { type: String, required: true },
    cp: { type: Number, required: true },
    latitude: { type: Number, required: true },
    longitude: { type: Number, required: true },
    day: { type: Number, required: true },
    datetime: { type: String, required: true },
    wind10m: { type: Number, required: true },
    gust10m: { type: Number, required: true },
    dirwind10m: { type: Number, required: true },
    rr10: { type: Number, required: true },
    rr1: { type: Number, required: true },
    probarain: { type: Number, required: true },
    weather: { type: Number, required: true },
    tmin: { type: Number, required: true },
    tmax: { type: Number, required: true },
    sun_hours: { type: Number, required: true },
    etp: { type: Number, required: true },
    probafrost: { type: Number, required: true },
    probafog: { type: Number, required: true },
    probawind70: { type: Number, required: true },
    probawind100: { type: Number, required: true },
    gustx: { type: Number, required: true },
});

module.exports = mongoose.model('Meteo', meteoSchema, 'meteo');