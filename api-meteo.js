const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const Meteo = require('./models/Meteo');

mongoose.connect('mongodb+srv://adoule:Xm7557557MongoDB@cluster0.pdstr.mongodb.net/testJardin?retryWrites=true&w=majority',
   {
      useNewUrlParser: true,
      useUnifiedTopology: true
   })
   .then(() => console.log('Connexion à MongoDB réussie !'))
   .catch(() => console.log('Connexion à MongoDB échouée !'));

const app = express();

app.use((req, res, next) => {
   res.setHeader('Access-Control-Allow-Origin', '*');
   res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
   res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
   next();
});



app.use(bodyParser.json());

app.use('/apimeteo/prev', (req, res, next) => {
   Meteo.find()
      .then(meteoAll => res.status(200).json(meteoAll))
      .catch(error => res.status(400).json({ error }));
});



function cumulPluieFctJour(condition) {
   return new Promise((resolve) => {
      Meteo.find(condition)
         .then(data => {
            resolve([data[0].rr10, condition["datetime"]["$regex"].substring(2,12)])
         })
         .catch(err => {
            resolve(["?", condition["datetime"]["$regex"].substring(2,12)])
         });
         
   });
}

app.get('/apimeteo/precipitation7j/:insee', (req, res, next) => {
   const insee = req.params.insee;
   const promises = [];

   for (let dateutilisee=7; dateutilisee>0;dateutilisee--){
      var d = new Date();
      d.setDate(d.getDate() - dateutilisee);     

      var condition = {
         "insee": insee,
         "datetime" : {"$regex" : ".*" + d.toJSON().substring(0, 10) + ".*"},
      }
      promises.push(cumulPluieFctJour(condition));

   }

   Promise.all(promises)
      .then((results) => {
         res.status(200).send({ precipitation7j: results});
      })
      .catch((e) => {
         res.status(500).send({
            message:
               e.message || "Erreur lors de la récupération des valeurs météorologiques"
            // Handle errors here
         });
      });

      
});

app.get('/apimeteo/precipitation/:insee/:datetime', (req, res, next) => {
   const insee = req.params.insee;
   const datetime = req.params.datetime;
   var condition = {
      "insee": insee,
      "datetime" : {"$regex" : ".*" + datetime + ".*"},
   }

   Meteo.find(condition)
      .then(data => {
         res.status(200).send({ precipitationday: data[0].rr10 });
      })
      .catch(err => {
         res.status(500).send({
            message:
               err.message || "Some error occurred while retrieving tutorials."
         });
      });
});

app.get('/apimeteo/vent/:insee/:datetime', (req, res, next) => {
   const insee = req.params.insee;
   const datetime = req.params.datetime;
   var condition = {
      "insee": insee,
      "datetime" : {"$regex" : ".*" + datetime + ".*"},
   }

   Meteo.find(condition)
      .then(data => {
         res.status(200).send({ wind10m: data[0].wind10m, gust10m: data[0].gust10m, dirwind10m: data[0].dirwind10m, probawind70: data[0].probawind70, probawind100: data[0].probawind100 });
      })
      .catch(err => {
         res.status(500).send({
            message:
               err.message || "Some error occurred while retrieving tutorials."
         });
      });
});

app.get('/apimeteo/gel/:insee/:datetime', (req, res, next) => {
   const insee = req.params.insee;
   const datetime = req.params.datetime;
   var condition = {
      "insee": insee,
      "datetime" : {"$regex" : ".*" + datetime + ".*"},
   }

   Meteo.find(condition)
      .then(data => {
         res.status(200).send({ probafrost: data[0].probafrost });
      })
      .catch(err => {
         res.status(500).send({
            message:
               err.message || "Some error occurred while retrieving tutorials."
         });
      });
});


app.get('/apimeteo/temperature/:insee/:datetime', (req, res, next) => {
   const insee = req.params.insee;
   const datetime = req.params.datetime;
   var condition = {
      "insee": insee,
      "datetime" : {"$regex" : ".*" + datetime + ".*"},
   }

   Meteo.find(condition)
      .then(data => {
         res.status(200).send({ tmin: data[0].tmin, tmax: data[0].tmax });
      })
      .catch(err => {
         res.status(500).send({
            message:
               err.message || "Some error occurred while retrieving tutorials."
         });
      });
});


app.get('/apimeteo/soleil/:insee/:datetime', (req, res, next) => {
   const insee = req.params.insee;
   const datetime = req.params.datetime;
   var condition = {
      "insee": insee,
      "datetime" : {"$regex" : ".*" + datetime + ".*"},
   }

   Meteo.find(condition)
      .then(data => {
         res.status(200).send({ sunHours: data[0].sun_hours });
      })
      .catch(err => {
         res.status(500).send({
            message:
               err.message || "Some error occurred while retrieving tutorials."
         });
      });
});


module.exports = app;
