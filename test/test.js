var should = require('chai').should(),
    supertest = require('supertest'),
    api = supertest('https://angejardin-nodejs-staging.herokuapp.com');


describe('/apimeteo/prev', function() {

  it('returns meteo as JSON', function(done) {
    api.get('/apimeteo/prev')
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) return done(err);
      done();
    });
  });

  it('returns err with bad path', function(done) {
    api.get('/apimeteo/prec')
    .expect(404)
    .end(function(err, res) {
      if (err) return done(err);
      done();
    });
  });


  it('returns meteo in array with all good elements', function(done) {
    api.get('/apimeteo/prev')
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) return done(err);
      res.body.should.be.instanceof(Array);
      for (let elt = 0; elt < res.body.length; elt++) {        
        res.body[elt].should.have.property("insee").and.to.be.a('string');;
        res.body[elt].should.have.property("cp").and.to.be.a('Number');;
        res.body[elt].should.have.property("latitude").and.to.be.a('Number');;
        res.body[elt].should.have.property("longitude").and.to.be.a('Number');;
        res.body[elt].should.have.property("day").and.to.be.a('Number');;
        res.body[elt].should.have.property("datetime").and.to.be.a('string');;
        res.body[elt].should.have.property("wind10m").and.to.be.a('Number');;
        res.body[elt].should.have.property("gust10m").and.to.be.a('Number');;
        res.body[elt].should.have.property("dirwind10m").and.to.be.a('Number');;
        res.body[elt].should.have.property("rr10").and.to.be.a('Number');;
        res.body[elt].should.have.property("rr1").and.to.be.a('Number');;
        res.body[elt].should.have.property("probarain").and.to.be.a('Number');;
        res.body[elt].should.have.property("weather").and.to.be.a('Number');;
        res.body[elt].should.have.property("tmin").and.to.be.a('Number');;
        res.body[elt].should.have.property("tmax").and.to.be.a('Number');;
        res.body[elt].should.have.property("sun_hours").and.to.be.a('Number');;
        res.body[elt].should.have.property("etp").and.to.be.a('Number');;
        res.body[elt].should.have.property("probafrost").and.to.be.a('Number');;
        res.body[elt].should.have.property("probafog").and.to.be.a('Number');;
        res.body[elt].should.have.property("probawind70").and.to.be.a('Number');;
        res.body[elt].should.have.property("probawind100").and.to.be.a('Number');;
        res.body[elt].should.have.property("gustx").and.to.be.a('Number');;
        }
      done();
    });
  });

  

})

describe('/apimeteo/precipitation7j', function() {

  it('returns JSON with property \'precipitation7j\'', function(done) {
    api.get('/apimeteo/precipitation7j/79249')
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) return done(err);
      res.body.should.have.property('precipitation7j');
      done();
    });
  });

});
